import discord
import json
import requests
import datetime
import concurrent
import asyncio
from os import path
from bot_logging import logger
from bot_util import get_config_value, create_lodestone_database
from bs4 import BeautifulSoup
from borel_enums import AchievementKind

# TODO consider finding a config way to determine the number of max workers.
executor = concurrent.futures.ThreadPoolExecutor(max_workers=10)

async def print_who_am_i(message):
    prefix = get_config_value('prefix')
    assoc_exists = association_exists(str(message.author.id))
    if(assoc_exists is True):
        prefix = get_config_value('prefix')
        association = get_association(str(message.author.id))
        if(association is None):
            await message.channel.send('Either an association doesn\'t exist between your Discord ID and a Lodestone ID (run lsadd first!) or something went wrong while trying to do the whoami command')
        else:
            lodestone_id = association['lodestone-id']
            region = association['region']
            dataset = get_lodestone_dataset(lodestone_id, region)
            if(dataset is None):
                await message.channel.send('Either the character doesn\'t exist, or the information provided with lsadd were incorrect. Run lsdelete to remove the association and try readding it with lsadd!')
            else:
                embed=discord.Embed(title=F'{dataset["name"]}', url=F'https://{region}.finalfantasyxiv.com/lodestone/character/{lodestone_id}', description=F'Level {dataset["level"]}')
                embed.set_author(name=F'{dataset["location"]}',icon_url=F'{dataset["icon"]}')
                embed.set_thumbnail(url=F"{dataset['portrait']}")
                dataset_counter = 0
                dataset_counter_total = 1
                statblock_counter = 1
                job_string = ''
                for stat_label in dataset['stats']:
                    if(dataset['stats'][stat_label] != '-'):
                        job_string += F'**{stat_label}**: {dataset["stats"][stat_label]}\n'
                        dataset_counter += 1
                        dataset_counter_total += 1
                        if(dataset_counter == 5 or dataset_counter_total == len(dataset['stats'])):
                            embed.add_field(name=F"Stat Block {statblock_counter}", value=F"{job_string}", inline=True)
                            dataset_counter = 0
                            statblock_counter += 1
                            job_string = ''
                    else:
                        continue
                # The "value" string contains an empty character so the split looks better.
                embed.add_field(name=":diamond_shape_with_a_dot_inside: :diamond_shape_with_a_dot_inside: :diamond_shape_with_a_dot_inside: ", value="‎", inline=False)
                for param_key, _ in dataset['params'].items():
                    value_string = ''
                    for key, value in dataset['params'][param_key].items():
                        value_string += F'**{key}**: {value}\n'
                    embed.add_field(name=F"{param_key}", value=F"{value_string}", inline=True)
                embed.set_footer(icon_url=F'{message.author.avatar_url}', text=F"Requested by {message.author.name}")
                await message.channel.send(embed=embed)
    else:
        prefix = get_config_value('prefix')
        await message.channel.send(F'I was unable to find an existing association between your discord ID and a Lodestone ID. Please run {prefix}lsadd first.')

async def print_community_wall(message):
    content_split = message.content.split(' ')
    entries = None
    region_code = get_config_value('lodestone.community-region')
    if(len(content_split) > 1):
        if(len(content_split[1]) != 2):
            await message.channel.send(F'The region argument can only be 2 letters (na or eu).')
        else:
            request_url = F'https://{content_split[1]}.finalfantasyxiv.com/lodestone'
            page = requests.get(request_url)
            soup = BeautifulSoup(page.content, 'html.parser')
            entries = soup.find('ul', {'class': 'side__list__box'})
            if(entries == None):
                await message.channel.send(F'"{content_split[1]}" appears to be an invalid Lodestone region (try na or eu). Using server default instead.')
                region_code = get_config_value('lodestone.community-region')
                request_url = F'https://{region_code}.finalfantasyxiv.com/lodestone'
                page = requests.get(request_url)
                soup = BeautifulSoup(page.content, 'html.parser')
                entries = soup.find('ul', {'class': 'side__list__box'})
            else:
                region_code = content_split[1]
    else:
        request_url = F'https://{region_code}.finalfantasyxiv.com/lodestone'
        page = requests.get(request_url)
        soup = BeautifulSoup(page.content, 'html.parser')
    entries = soup.find('ul', {'class': 'side__list__box'}).findAll('div', {'class': 'entry more_'})
    embed=discord.Embed(title=F'Community Wall ({region_code.upper()})', url=F'https://{region_code}.finalfantasyxiv.com/lodestone/community')
    embed.set_author(name='Lodestone')
    embed.set_thumbnail(url="https://img.finalfantasyxiv.com/lds/h/L/cxcD5kjeM52JRVwrrzIF4dZNe0.png")
    for entry in entries:
        epoch_time = int(entry.find('time', {'class': 'entry__activity__time'}).find('span', {'class':'datetime_dynamic_ymdhm'})['data-epoch'])
        converted_time = datetime.datetime.fromtimestamp(epoch_time).strftime('%Y-%m-%d %H:%M:%S')
        entry_href = F'https://{region_code}.finalfantasyxiv.com' + entry.find('a', {'class': 'entry__activity'})['href']
        entry_author = entry.find('p', {'class':'entry__activity__data--name'})
        if(entry_author is None):
            activity_text = entry.find('div', {'class': 'entry__activity__txt--message'}).p.text
            embed.add_field(name=F"{converted_time}", value=F"[{activity_text}]({entry_href})", inline=False)
        else:
            entry_text = entry.find('div', {'class': 'entry__activity__txt--message'}).p.text
            embed.add_field(name=F"{converted_time}", value=F"[{entry_text}]({entry_href})", inline=False)
    embed.set_footer(icon_url=F'{message.author.avatar_url}', text=F"Requested by {message.author.name}")
    await message.channel.send(embed=embed)

async def set_nickname_to_character_name(message):
    assoc_exists = association_exists(str(message.author.id))
    if(assoc_exists is None):
        prefix = get_config_value('prefix')
        await message.channel.send(F'I was unable to find an existing association between your discord ID and a Lodestone ID. Please run {prefix}lsadd first.')
    else:
        association = get_association(str(message.author.id))
        character_name = get_character_name(association['lodestone-id'], association['region'])
        if(character_name is None):
            await message.channel.send('Either the character doesn\'t exist, or the information provided with lsadd were incorrect. Run lsdelete to remove the association and try readding it with lsadd!')
        else:
            try:
                await message.author.edit(nick=character_name, reason='Member requested nickname change to their associated Lodestone character name.')
                await message.channel.send(F'Grats on your new nickname {message.author.name}! Or should I say {message.author.nick}? :sunglasses:')
            except discord.errors.Forbidden as forb:
                await message.channel.send(F'I\'m sorry @{message.author.name}, I have insufficient permissions to set your nickname.')
                logger.exception(forb)

async def clear_ls_nickname(message):
    try:
        await message.author.edit(nick=None, reason='Member requested nickname clear.')
        await message.channel.send(F'I\'ve cleared your nickname successfully {message.author.name}!')
    except discord.errors.Forbidden as forb:
        await message.channel.send(F'I\'m sorry @{message.author.name}, I have insufficient permissions to set your nickname.')
        logger.exception(forb)

def get_character_name(lodestone_id: str, region: str) -> str:
    request_url = F'https://{region}.finalfantasyxiv.com/lodestone/character/{lodestone_id}'
    page = requests.get(request_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    if(soup.find('body', {'class': 'error__body'}) != None):
        return None
    else:
        character_name = soup.find('p', {'class':'frame__chara__name'}).string
        return character_name

def get_lodestone_dataset(lodestone_id: str, region: str) -> object:
    request_url = F'https://{region}.finalfantasyxiv.com/lodestone/character/{lodestone_id}'
    page = requests.get(request_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    if(soup.find('body', {'class': 'error__body'}) != None):
        return None
    else:
        character_portrait_link = soup.find('div', {'class':'character__detail__image'}).a['href'].split('?')[0]
        character_icon = soup.find('div', {'class': 'frame__chara__face'}).img['src'].split('?')[0]
        character_name = soup.find('p', {'class':'frame__chara__name'}).string
        character_level = soup.find('div', {'class':'character__class__data'}).p.string.split(' ')[1]
        character_location = soup.find('p', {'class':'frame__chara__world'}).text
        character_stat_list = soup.findAll('div', {'class':'character__level__list'})
        character_info = {}
        character_info['portrait'] = character_portrait_link
        character_info['icon'] = character_icon
        character_info['name'] = F"{character_name}"
        character_info['level'] = character_level
        character_info['location'] = character_location
        character_info['stats'] = {}
        character_info['params'] = {}
        for level_list in character_stat_list:
            for child in level_list.ul.children:
                key = ''
                value = ''
                for innerChild in child.children:
                    if(innerChild.string is None):
                        key = innerChild['data-tooltip']
                    else:
                        value = innerChild.string
                character_info['stats'][key] = value
        param_headers = soup.findAll('h3', {'class':'heading--lead'})
        param_list = soup.findAll('table', {'class':'character__param__list'})
        for index in range(len(param_headers)):
            for header_child in param_headers[index].children:
                if(header_child.string is not None):
                    header = header_child.string
                    character_info['params'][header] = {}
                    for row in param_list[index].children:
                        label = row.span.string
                        value = row.td.string
                        character_info['params'][header][label] = value
        return character_info

async def set_lodestone_roles(message):
    try:
        await message.channel.send('This may take a long time. Please do not run the command again until I respond.')
        association = get_association(str(message.author.id))
        if(association is None):
            prefix = get_config_value('prefix')
            await message.channel.send(F'I was unable to find an existing association between your discord ID and a Lodestone ID. Please run {prefix}lsadd first.')
        else:
            achievement_roles = get_config_value('achievements')
            achievement_types = []
            for key, value in achievement_roles.items():
                if(len(value) > 0):
                    achievement_types.append(key)
            if(len(achievement_types) > 0):
                # this pool is used to avoid this being a blocking call as
                # the operation can take a long time. Without this, no one
                # else can use the bot while the processing is happening.
                loop = asyncio.get_event_loop()
                achievements = await loop.run_in_executor(executor, get_lodestone_achievements, association['lodestone-id'], association['region'], achievement_types)
                if(achievements is None or len(achievements) == 0):
                    await message.channel.send(
                        F'I couldn\'t find any achievements {message.author.name}! Make sure they are marked as Public on your Lodestone profile!' +
                        'Alternatively, achievmeents may take a while to update on the page, so try again a little later!')
                else:
                    role_changes = {}
                    role_changes['new'] = []
                    role_changes['replace'] = []
                    role_changes['remove'] = []
                    config_roles = get_config_value('achievements')
                    # removing unused keys first
                    for key in achievements.keys():
                        if(key not in config_roles.keys()):
                            config_roles[key.split('.')[1]].pop()
                    # now iterate over the config values
                    for achievement_kind, values in config_roles.items():
                        for list_item in values:
                            # there is only 1 achievement to check
                            if(len(list_item['labels']) == 1):
                                if(list_item['labels'][0] in achievements[F'AchievementKind.{achievement_kind}']):
                                    if(list_item['replaces'] == '' or list_item['replaces'] is None):
                                        role_changes['new'].append(list_item['role'])
                                    else:
                                        role_changes['replace'].append({'old': list_item['replaces'], 'new': list_item['role']})
                            # else there are multiple achievements to check for this role
                            else:
                                label_count = 0
                                for label in list_item['labels']:
                                    if(label in achievements[F'AchievementKind.{achievement_kind}']):
                                        label_count += 1
                                    else:
                                        break
                                # we have met all necessary achievement requirements to get
                                # the associated role if this passes.
                                if(label_count == len(list_item['labels'])):
                                    if(list_item['replaces'] == '' or list_item['replaces'] is None):
                                        role_changes['new'].append(list_item['role'])
                                    else:
                                        role_changes['replace'].append({'old': list_item['replaces'], 'new': list_item['role']})
                    if(len(role_changes['new']) > 0):
                        # if we have new roles to add
                        # we better make sure they weren't superceded
                        # by another role first
                        if(len(role_changes['replace']) > 0):
                            for new_role in role_changes['new']:
                                for role_obj in role_changes['replace']:
                                    if(new_role == role_obj['old']):
                                        role_changes['remove'].append(role_obj['old'])
                                        role_changes_new_index = role_changes['new'].index(role_obj['old'])
                                        role_changes['new'][role_changes_new_index] = role_obj['new']
                    # we only have replacements to do
                    if(len(role_changes['new']) == 0 and len(role_changes['replace']) > 0):
                        for role_obj in role_changes['replace']:
                            role_changes['remove'].append(role_obj['old'])
                            role_changes['new'].append(role_obj['new'])
                    # now remove all roles that needs removing, and
                    # add all roles that needs adding.
                    # make sure to check for the users existing roles 
                    # first to save on Discord API calls
                    server_roles = message.guild.roles
                    author_roles = set(message.author.roles)
                    removal_role_list = []
                    new_role_list = []
                    # remove roles from the new list if the author already has those.
                    for author_role in author_roles:
                        if(len(role_changes['new']) == 0):
                            break
                        elif(author_role.name in role_changes['new']):
                            role_changes['new'].remove(author_role.name)
                    for server_role in server_roles:
                        if(server_role.name in role_changes['remove']):
                            removal_role_list.append(server_role)
                        if(server_role.name in role_changes['new']):
                            new_role_list.append(server_role)
                    for removal_role in removal_role_list:
                        try:
                            author_roles.remove(removal_role)
                        except KeyError:
                            continue
                    author_roles.update(new_role_list)
                    await message.author.edit(roles=list(author_roles), reason='User ran command to check achievements on the Lodestone to set server roles.')
                    await message.channel.send(F'I\'m all done {message.author.name}! If you gained any server roles from achievements, they should be there now :)')
            else:
                await message.channel.send('The admins of this server haven\'t set up any roles to get through achievements. Sorry!')
    except discord.errors.Forbidden as forb:
        await message.channel.send(F'I\'m sorry {message.author.name}, I have insufficient permissions to set your server roles.')
        logger.exception(forb)
    except Exception as exc:
        print(F'Something went wrong while trying to set roles using lodestone achievements.')
        await message.channel.send('Sorry champ, but something went wrong while I went to check your achievements.')
        logger.exception(exc)

async def remove_lodestone_roles(message):
    try:
        config_roles = get_config_value('achievements')
        role_labels = set()
        for _, role_list in config_roles.items():
            if(len(role_list) > 0):
                for role_obj in role_list:
                    role_labels.add(role_obj['role'])
                    role_labels.add(role_obj['replaces'])
        converted_roles = []
        guild_roles = message.guild.roles
        author_roles = set(message.author.roles)
        for guild_role in guild_roles:
            if(guild_role.name in role_labels):
                converted_roles.append(guild_role)
            for role in converted_roles:
                try:
                    author_roles.remove(role)
                except KeyError:
                    continue
        await message.author.edit(roles=author_roles, reason='User called command to clear Lodestone Achievement given roles')
        await message.channel.send(F'There you go {message.author.name}. Your Lodestone Achievement roles should be gone now!')
    except discord.errors.Forbidden as forb:
        await message.channel.send(F'I\'m sorry {message.author.name}, I have insufficient permissions to clear your roles.')
        logger.exception(forb)
    except Exception as exc:
        print(F'Something went wrong while trying to clear your lodestone server roles.')
        await message.channel.send('Sorry champ, but something went wrong while I tried to clear your roles.')
        logger.exception(exc)

def get_lodestone_achievements(lodestone_id: str, region: str, achievement_types: list) -> object:
    '''Function used to retrieve achievements on a profile page (if any)
    
    If no achievements are found this functionn returns None. Achievements 
    must be public on the players profile page to retrieve them.
    '''
    try:
        request_url = F'https://{region}.finalfantasyxiv.com/lodestone/character/{lodestone_id}/achievement/'
        page = requests.get(request_url)
        soup = BeautifulSoup(page.content, 'html.parser')
        if(soup.find('body', {'class': 'error__body'}) != None):
            return None
        else:
            achievements = {}
            for enum_pair in AchievementKind:
                if(enum_pair.name in achievement_types):
                    print(F'Processing {enum_pair.name}...')
                    achievements[F'AchievementKind.{enum_pair.name}'] = []
                    achievement_page_request = F'https://{region}.finalfantasyxiv.com/lodestone/character/{lodestone_id}/achievement/kind/{enum_pair.value}/?filter=2'
                    achievement_page = requests.get(achievement_page_request)
                    achievement_soup = BeautifulSoup(achievement_page.content, 'html.parser')
                    for achievement in achievement_soup.findAll('li', {'class': 'entry'}):
                        entry = achievement.a.find('div', {'class','entry__achievement--list entry__achievement--history'}).find('p', {'class':'entry__activity__txt'}).text
                        achievements[F'AchievementKind.{enum_pair.name}'].append(entry)
                    achievements[F'AchievementKind.{enum_pair.name}'] = list(set(achievements[F'AchievementKind.{enum_pair.name}']))
                    print(F'Done!')
                else:
                    continue
            return achievements
    except Exception as exc:
        print('Something went wrong while trying to retrieve achievements from the lodestone website. Check the log for details.')
        logger.exception(exc)
        return None

def get_association(discord_id: str) -> object:
    '''Function used to retrieve an existing Association between a Discord ID and Lodestone ID
    
    This function does no validation on the input and expects only valid input.
    It will return the object if it exists, otherwise None.
    '''
    try:
        lodestone_path = get_config_value('lodestone.path')
        with open(lodestone_path, 'r') as json_data:
            lodestone_db = json.load(json_data)
        for association in lodestone_db['associations']:
            if(association['discord-id'] == discord_id):
                return { 'lodestone-id': association['lodestone-id'], 'region': association['region'] }
        return None
    except Exception as exc:
        print('Something went wrong while trying to retrieve an association from the local lodestone database.')
        logger.exception(exc)
        return None

async def associate_user_with_id(message):
    lodestone_path = get_config_value('lodestone.path')
    if(path.isfile(lodestone_path) == True):
        id_exists = association_exists(str(message.author.id))
        if(id_exists == False):
            try:
                prefix = get_config_value('prefix')
                sub_string = message.content[len(F'{prefix}lsadd'):len(message.content)].strip()
                split_values = sub_string.split(' ')
                if(split_values[1].lower() != 'na' and split_values[1].lower() != 'eu'):
                    await message.channel.send('Invalid region provided. Has to be eu or na.')
                else:
                    with open(lodestone_path, 'r') as json_data:
                        lodestone_db = json.load(json_data)
                    new_entry = { 'discord-id': F'{message.author.id}', 'lodestone-id': split_values[0], 'region': split_values[1].lower() }
                    lodestone_db['associations'].append(new_entry)
                    lodestone_db['entries'] = len(lodestone_db['associations'])
                    with open(lodestone_path, 'w') as json_data:
                        json.dump(lodestone_db, json_data, indent=4)
                    await message.channel.send(F'Discord ID was successfully associated with a Lodestone ID! You can now call {prefix}whoami')
            except Exception as exc:
                await message.channel.send('Something went wrong when I tried to associate your Discord ID with a Lodestone ID.')
                logger.exception(exc)
        elif(id_exists == True):
            await message.channel.send('This Discord ID is already associated with a character. Remove it first, then re-associate it with the new ID.')
        else:
            await message.channel.send('Something went wrong while checking if the association already exists.')
    else:
        await message.channel.send('It appears that the local lodestone database does not exist.')

async def delete_user_association(message):
    lodestone_path = get_config_value('lodestone.path')
    if(path.isfile(lodestone_path) == True):
        id_exists = association_exists(str(message.author.id))
        if(id_exists == True):
            try:
                prefix = get_config_value('prefix')
                with open(lodestone_path, 'r') as json_data:
                    lodestone_db = json.load(json_data)
                associations = lodestone_db['associations']
                associations[:] = [dictionary for dictionary in associations if dictionary.get('discord-id') != str(message.author.id)]
                lodestone_db['associations'] = associations
                with open(lodestone_path, 'w') as json_data:
                    json.dump(lodestone_db, json_data, indent=4)
                await message.channel.send(F'The disassociation was successfully done!')
            except Exception as exc:
                await message.channel.send('Something went wrong when I tried to disassociate your Discord ID from your Lodestone ID.')
                logger.exception(exc)
        elif(id_exists == False):
            try:
                prefix = get_config_value('prefix')
                await message.channel.send(F'This Discord ID have not yet been associated with a Lodestone ID. Please run the {prefix}lsadd command first.')
            except Exception as exc:
                print('An exception occurred while trying to get the prefix.')
                logger.exception(exc)
                await message.channel.send(F'This Discord ID have not yet been associated with a Lodestone ID. Please run the lsadd command first.')
        else:
            await message.channel.send('Something went wrong while checking if the association already exists.')
    else:
        await message.channel.send('It appears that the local lodestone database does not exist.')

def association_exists(discord_id: str) -> bool:
    try:
        lodestone_path = get_config_value('lodestone.path')
        if(path.isfile(lodestone_path) == True):
            with open(lodestone_path, 'r') as json_data:
                lodestone_db = json.load(json_data)
            lodestone_db_list = lodestone_db['associations']
            if(len(lodestone_db_list) is 0):
                return False
            else:
                for association in lodestone_db_list:
                    if(association['discord-id'] == discord_id):
                        return True
                return False
        else:
            return None
    except Exception as exc:
        print('An exception occurred while seeing if an Association exists in the lodestone db.')
        logger.exception(exc)
        return None
    