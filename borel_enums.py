from enum import Enum

class AchievementKind(Enum):
    BATTLE = 1
    PVP = 2
    CHARACTER = 3
    ITEMS = 4
    CRAFTING_AND_GATHERING = 5
    QUESTS = 8
    EXPLORATION = 11
    GRAND_COMPANY = 12