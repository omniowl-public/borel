import discord
import json
from bot_logging import logger
from bot_util import get_config_value

async def eval_item_info_input(bot_user, message):
    sub_string = message.content[len('!iteminfo'):len(message.content)].strip()
    lookup = {}
    if(sub_string.isnumeric() == True):
        lookup = get_item_basic_id(sub_string)
    else:
        lookup = get_item_basic_name(sub_string)
    if(lookup is not None):
        embed=discord.Embed(title=F"{lookup['originalName']}", url=F"https://universalis.app/market/{lookup['id']}", description=F"{lookup['description']}")
        embed.set_author(name="Borel", url="https://gitlab.com/omniowl-public/borel", icon_url=F"{bot_user.avatar_url}")
        embed.set_thumbnail(url=F"https://universalis.app/i/icon2x/{lookup['id']}.png")
        embed.add_field(name="Stacksize", value=F"{lookup['stacksize']}", inline=False)
        embed.add_field(name="Unique", value=F"{'Yes' if lookup['isUnique'] == 'True' else 'No'}", inline=False)
        embed.add_field(name="Tradeable", value=F"{'Yes' if lookup['isUntradeable'] == 'True' else 'No'}", inline=False)
        embed.add_field(name="Indisposable", value=F"{'Yes' if lookup['isIndisposable'] == 'True' else 'No'}", inline=False)
        embed.add_field(name="Can be Hq", value=F"{'Yes' if lookup['canBeHq'] == 'True' else 'No'}", inline=False)
        embed.add_field(name="Price (Mid): ", value=F"{lookup['price_mid']}", inline=False)
        embed.set_footer(text="Data Provided by Universalis & XIVAPI")
        await message.channel.send(embed=embed)
    else:
        await message.channel.send('I didn\'t find an item with that name/id. Check your spelling and try again!')

def get_item_basic_name(item_name: str) -> object:
    db_path = get_config_value('itemdb.db-path')
    name = item_name.lower()
    try:
        with open(db_path) as json_data:
            itemdb = json.load(json_data)
            return itemdb[name]
    except Exception as exc:
        print('Something went wrong while searching for the item: ' + item_name)
        logger.exception(exc)
    return None

def get_item_basic_id(item_id: str) -> object:
    db_path = get_config_value('itemdb.db-path')
    try:
        with open(db_path) as json_data:
            itemdb = json.load(json_data)
            for item in itemdb:
                if(item_id == itemdb[item]["id"]):
                    return itemdb[item]
    except Exception as exc:
        print('Something went wrong while searching for the item: ' + item_id)
        logger.exception(exc)
    return None