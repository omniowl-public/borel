import discord
import json
import pandas
from os import path
from datetime import date
from bot_logging import logger

def get_config_value(jsonIdentifier: str) -> object:
    '''A utility function used to get a json value from the config.json file.

    If this call fails for any reason, it will return None instead of the
    requested value.
    '''
    try:
        with open('config.json') as json_data:
            config = json.load(json_data)
            split = jsonIdentifier.split('.')
            result = config
            for index in range(len(split)):
                result = result[split[index]]
                if(index == len(split) - 1):
                    return result
    except Exception as exc:
        # graceful error handling
        print('Something went wrong while trying to retrieve a value from the json file.')
        print(exc)
        if(logger is not None):
            logger.error('Something went wrong while trying to retrieve a value from the json file.')
            logger.exception(exc)
    return None

def fetch_item_database() -> bool:
    '''Creates a small local item database as a lookup tool. Returns true if successful.

    The CSV file that is retrieved exists on the XIVAPI github.
    It's simply data consisting of items and their basic descriptions.
    ONLY RUN THIS FUNCTION SPARRINGLY.
    '''
    print('Attempting to fetch Item Database...')
    try:
        sheet_url = get_config_value('itemdb.sheet-path')
        csvData = pandas.read_csv(sheet_url, low_memory=False)
        # csv keys:
        # 'key' = id
        # 9 = name
        # 8 = description
        # 19 = stacksize
        # 20 = IsUnique
        # 21 = IsUntradable
        # 22 = IsIndisposable
        # 24 = Price(Mid)
        # 26 = CanBeHq
        csv_slice = csvData[['key', '9', '8', '19', '20', '21', '22', '24', '26']]
        csv_slice = csv_slice[csv_slice['9'] != '']
        csv_slice = csv_slice[csv_slice['9'].notna()]
        json_data = {}
        for index, row in csv_slice.iterrows():
            if(index > 2):
                row_name = row['9'].lower()
                json_data[row_name] = { 
                    "id": row['key'],
                    "originalName": row['9'],
                    "description": "",
                    "stacksize": row['19'],
                    "isUnique": row['20'],
                    "isUntradeable": row['21'],
                    "isIndisposable": row['22'],
                    "canBeHq": row['26'],
                    "price_mid": row['24'] 
                }
                if(pandas.notna(row['8']) == True):
                    json_data[row_name]['description'] = row['8']
        jsonFile = get_config_value('itemdb.db-path')
        with open(jsonFile, 'w') as jFile:
            jFile.write(json.dumps(json_data, indent=4))
    except Exception as exc:
        print('Something went wrong while retrieving the Item Database.')
        logger.exception(exc)
        return False
    return True

def create_lodestone_database():
    lodestone_path = get_config_value('lodestone.path')
    if(lodestone_path is not None):
        if(path.isfile(lodestone_path) == False):
            print('No Lodestone Database found. Creating a new one!')
            logger.info('No Lodestone Database found. Creating a new one!')
            lodestone_db = {}
            lodestone_db['created'] = str(date.today())
            lodestone_db['entries'] = 0
            lodestone_db['associations'] = []
            with open(lodestone_path, 'w') as jsonFile:
                json.dump(lodestone_db, jsonFile, indent=4)
            return
    else:
        print('No lodestone.path found! Lodestone lookup cannot work without it!')
        logger.error('No lodestone.path found! Lodestone lookup cannot work without it!')
        return

async def bot_help(bot_user, message):
    embed=discord.Embed(title='Command Guide', description=F'A short description of each available Borel Bot command on the {message.guild.name} server.')
    embed.set_author(name='Borel', url='https://gitlab.com/omniowl-public/borel', icon_url=F'{bot_user.avatar_url}')
    embed.add_field(name='iteminfo <item_id>/<name>', value='Will print a basic embed overview of an item with the id or name provided. Example Usage: **"iteminfo 26465"** or **"iteminfo Facet Tabard of Aiming"**', inline=False)
    embed.add_field(name='marketboard <datacenter> <item_id>/<name>', value='Will print an overview of the requested item, with average prices, qualities, and sellers. Example Usage: **"marketboard Crystal 5111"** or **"marketboard Crystal Iron Ore"**', inline=False)
    embed.add_field(name='mb datacenter <item_id>/<name>',value='This is a shorthand version of the *marketboard* command.',inline=False)
    embed.add_field(name='lsadd <lodestone_id> <region_code>',value='This command is used to make an association between your discord id and your lodestone character. Look further down for an explanation. Example Usage: "lsadd 25863382 na',inline=False)
    embed.add_field(name='lsdelete/lsremove',value='This command removes your discord and lodestone association from the server.',inline=False)
    embed.add_field(name='whoami',value='Checks the Lodestone website for your characters information and prints it in chat. Requires to have run **lsadd command first**.',inline=False)
    embed.add_field(name='lswall',value='Prints the Lodestone website Community Wall in the chat.',inline=False)
    embed.add_field(name='lsachievements',value='Will check the Lodestone website for Achievements (**they have to be public on your profile**) and then gives you discord roles that are associated with those achievements (if any). Requires to have run **lsadd command first**.',inline=False)
    embed.add_field(name='lsclearachievements',value='Will remove all server roles given to you that were associated with Lodestone Achievements.',inline=False)
    embed.add_field(name='lsnick',value='Will set your Discord nickname to that of your characters name. Requires to have run the **lsadd command first**.',inline=False)
    embed.add_field(name='lsclearnick',value='Will reset your Discord nickname.',inline=False)
    embed.set_footer(text="Borel is Open Source on Gitlab. Click his name at the top to get to the repository!")
    await message.author.send(embed=embed)
    lsadd_explained = '**lsadd Explained**\n'
    lsadd_explained += 'Go to the Lodestone Website and locate your character. Once you\'ve got the website link to your character note down the string of numbers, seen after the ".../character/" portion of the websites URL. '
    lsadd_explained += 'A character URL could look like this: https://na.finalfantasyxiv.com/lodestone/character/25863382 \n'
    lsadd_explained += 'So when running lsadd you would also have to specify your region. You can either write "na" (for North America) or "eu" (for Europe). In this case, that would mean the lsadd command call would look like this:\n'
    lsadd_explained += 'lsadd 25863382 na'
    await message.author.send(lsadd_explained)