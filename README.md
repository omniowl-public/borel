# Borel

## Features
Just to get the features out of the way, here you go:
* Marketboard - Look up information about items and their prices using the 
[Universalis API](https://universalis.app/).
* Lodestone - Print out character data, straight from the official Lodestone 
website right into Discord.
* Server Roles Based on Achievements - A simple command makes it possible to have 
the bot check the Lodestone website for achievements, and then grant server roles 
based on those achievements!
* Lodestone Nicknames - Be your character wherever you go. Use a simple command 
to set your Discord nickname to that of your character's retrieved straight from 
the Lodestone website automagically!
* Community Wall - Wanna stay up-to-date on the newest community happenings? Use 
a command to post the 10 newest entries from the official Lodestone Website right 
from the comfort of your Discord server

More features coming as time goes on. Take a look at the 
[issue tracker](https://gitlab.com/omniowl-public/borel/-/issues) so you are 
always up-to-date on upcoming features and known issues.

## Introduction
In 2020 a guy named Lethys (Yandawl) was exposed as a dude misusing his highly 
successful Discord bot named "Ser Aymeric" to spy on people he didn't like. This 
came out as a [whistleblower on Tumblr](https://seraymeric-discord-spyware.tumblr.com/)
showcased screenshots to back up their claims that indeed this person was using 
their Discord Bot to freely engage in very shady behaviour, by catalouging and 
storing thousands upon thousands of messages. Most of these messages were 
written in private channels, and no one was the wiser.

The problem is however, that a big chunk of the FFXIV community actually uses 
this bot as it is very feature-rich. It's a shame really that a predatory developer
has to poison their own well.

So to combat this, I have decided to make an open-source version to the best of my
abilities. As someone who has never played the game, I decided to go on a Google 
search adventure to find out what to call the bot and I ended on "Borel". According 
to fan-made wikis "Borel" is a family from which the character "Ser Aymeric" is 
from. But more interestingly, his weapon "Naegling" is an heirloom of said family, 
which was once used to slay Dragons...so it seems fitting :-)

## Goals
This bot is developed with usability in mind for people who don't program, and 
to only provide niche functionality. Things that the FFXIV community actually 
cares about. If you want a general purpose bot, then there is a whole swath of 
those online like Dyno, YAGPDB and others. No need to re-invent that particular 
wheel!

## Setup
Currently the local environment for development is set up, by following the official
discord.py guidelines for prerequisites. So before you get started, head over to 
the [website](https://discordpy.readthedocs.io/en/latest/intro.html#prerequisites)
and follow the instructions for setup.

As of writing this (02/07/2020) I am using **discord.py 1.4** and **Python 3.7.2** 
on **Windows 10**. I am also making use of external modules. Below is the list 
which will likely update over time, so make sure to keep an eye on it when new
updates arrive to the code:
* [requests](https://requests.readthedocs.io/en/master/) - Used for simple HTTP Requests
* [getch](https://github.com/joeyespo/py-getch) - A portalable Python Getch Library to call pause() and pause_exit() easily
* [pandas](https://pandas.pydata.org/) - A data analysis tool used to get CSV/Python data from APIs and Websites
* [beautifulsoup4](https://pypi.org/project/beautifulsoup4/) - A library used for easy Webscraping. This is used to get data from the lodestone website.

## Config
This bot makes use of a config to make it easily accessible even to non-programmers. 
From this config you can control quite a few things about the bot and how it operates. 
Seeing as the config used for development and/or server deployment should not be 
available on a code repo, I have instead made a config_template.json file which 
is updated as the development config in use is updated to mirror it. When you get 
this bot, rename the template to config.json and fill in the appropriate fields. 
Some of the fields are not secret and will be prefilled for you. The fields you 
have to fill in yourself are currently as follows:
* secret - The token you get from the Discord Developer Portal. Make sure to keep 
this token only visible to those who absolutely need to know it.

### itemdb
The itemdb object has to do with creating a local file to use as a lookup table 
for item names. Without it, a user could only look up items by knowing its id. 
It has three fields:
* sheet-path -  The path to an online CSV file on the XIVAPI github. The code 
will use the pandas library to retrieve the CSV file and then turn it into a 
local usable json file.
* db-path - This is where the resulting item database file is stored.
* force-update - If this flag is true it will always update the item database 
every time the bot is run rather than reuse what might already be on disk.
**DO NOT LEAVE THIS FLAG AS TRUE WHEN THE BOT IS DEPLOYED ON A SERVER**. It's a 
fairly costly operation.

### logging
The logging object has a few pieces of information used to decide where to store 
bog event logs, what level to log, whether to log at all and how big the log folder 
can get before giving a warning to clean out old logs:
* isActive - If this is true, the bot will have logs. If it's false, it won't have logs.
* level - Which level of warnings to log. Flags can be found [here](https://docs.python.org/3/library/logging.html#logging-levels)
* path - Where to store the logs that it creates. A log is only created on startup of the bot.
* warning-size - How many megabytes the folder can be for logs before the bot warns the user about it

### lodestone
The lodestone object is used to store information about how to handle the local 
lodestone association database as well as other lodestone relevant parameters. 
These associations consists of a discord id, a lodestone id 
(obtained from the official lodestone character website) and a region (na or eu):
* path - The path to the json file that will store lodestone-discord id 
associations locally.
* community-region - The default region to use for the 'lswall' command. It should 
either be 'na' or 'eu'.

### achievements
The achievements object is used to store information about what server roles can be 
granted by the bot to users, based on what achievements the user of the command has on 
the official Lodestone website. **The config object is pre-configured with all types of existing achievement types and this should not be changed**. However, each of those categories is a list that can hold objects. An example of this is shown below:
```json
"BATTLE":
[
    {
        "labels":["To Crush Your Enemies I"],
        "role": "battle-1",
        "replaces":""
    },
    {
        "labels":["To Crush Your Enemies II"],
        "role": "battle-2",
        "replaces": "battle-1"
    },
    {
        "labels":["To Crush Your Enemies III"],
        "role": "battle-3",
        "replaces": "battle-2"
    }
]
```
* labels - A comma separated list of Achievements from the Lodestone that can give the server role specified in the "role" attribute. **Please Note**: if multiple achievements are put in the "labels" list, then it will require the user to have **all** of the listed achievements to get the role.
* role - The server role that will be given, if the user of the command has all of the achievments found in the "labels" list.
* replaces - This is an optional parameter (though leave it empty if not used as seen in the example) which will replace an existing server role on the user, in case one role supersedes another.
It is **very important** that the achievement names and server roles are spelled exactly as they are on the Lodestone website and Discord Server respectively as otherwise no roles can be added. If special characters are used that aren't ASCII, **you have to** use a unicode character instead. An example is given below:
```
This:         "Pièce de Résistance: Akademos"
Becomes this: "Pi\u00e8ce de R\u00e9sistance: Akademos"
```
This is due to the nature of how JSON stores unicode characters. Nothing I can do about that. Sorry!
To help out with this, [here is a website to convert special characters to Unicode characters](https://www.branah.com/unicode-converter).

# Thanks
Underneath you'll find a list of people and teams who helped the creation of 
this bot in one way or another (unless stated, none of the listed actively 
endorse the development of the bot, nor is affiliated with its development):
* [discord.py Team](https://discordpy.readthedocs.io/en/latest/) - For making the framework used to power this discord bot.
* [@sassichi](https://twitter.com/sassichi) - Helped explaining how the Marketboard and Lodestone lookup commands work from a user perspective.
* [karashiiro](https://github.com/karashiiro) - For making the [Universalis API](https://universalis.app/) that's used to look up the Marketboard information.
* [@Swordians](https://twitter.com/Swordians) - Helped explaining how the nickname and achievement functions work.
* Izora - A great personal friend on Discord for helping out testing bot functionality.