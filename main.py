import discord
import json
import logging
import time
import sys
from os import path
from getch import pause_exit
from pathlib import Path
from bot_util import (
    bot_help,
    create_lodestone_database,
    fetch_item_database, 
    get_config_value
)
from bot_logging import logger
from item_info import eval_item_info_input
from market_board import eval_mb_input
# lodestone imports sorted alphabetically as there are many imports from this module
# TODO consider making a static-like setup so fewer imports are needed
from lodestone import (
    associate_user_with_id, 
    clear_ls_nickname,
    delete_user_association,
    print_community_wall,
    print_who_am_i,
    remove_lodestone_roles,
    set_lodestone_roles,
    set_nickname_to_character_name
)

client = discord.Client()

try:
    # if this test fails, then there is no config file or
    # its malformed/misplaced. The bot can't run without it.
    with open('config.json') as json_data:
        json.load(json_data)
except Exception as exc:
    print('A problem occurred while reading the config file. Aborting.')
    print(exc)
    pause_exit(0, 'Press Any Key To Exit.')

@client.event
async def on_ready():
    print('Logged in as {0.user}'.format(client))
    logging_active = get_config_value('logging.isActive')
    if(logging_active is None):
        print('The isActive flag could not be retrieved from the config. Defaulting to false.')
        logging_active = False
    if(logging_active == True):
        print('Setting up Logging...')
        start_log()
    else:
        print('logging.isActive was set to false. No Logging.')
    print('Checking the Item Database...')
    itemdb_path = get_config_value('itemdb.db-path')
    if(itemdb_path is not None):
        itemdb_force = get_config_value('itemdb.force-update')
        if(itemdb_force is not None and itemdb_force == True):
            print('Forcing Item Database refresh...')
            fetch_item_database()
        else:
            if(path.isfile(itemdb_path) == False):
                print('No Item Database found. Generating a new one!')
                fetch_item_database()
            else:
                print('Item Database is fine!')
    else:
        print('item.db was None. Either there is no Item database to retrieve, or it wasn\'t put in there.')
    create_lodestone_database()
    print('Ready to go.')
    

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    msg_prefix = get_config_value('prefix')
    if(message.content.startswith(msg_prefix)):
        if('help' in message.content):
            await bot_help(client.user, message)
        elif('iteminfo' in message.content):
            await eval_item_info_input(client.user, message)
        elif('marketboard' in message.content or 'mb' in message.content):
            await eval_mb_input(client.user, message)
        elif('lsadd' in message.content):
            await associate_user_with_id(message)
        elif('lsdelete' in message.content or 'lsremove' in message.content):
            await delete_user_association(message)
        elif('whoami' in message.content):
            await print_who_am_i(message)
        elif('lswall' in message.content):
            await print_community_wall(message)
        elif('lsachievements' in message.content):
            await set_lodestone_roles(message)
        elif('lsclearachievements' in message.content):
            await remove_lodestone_roles(message)
        elif('lsnick' == message.content):
            await set_nickname_to_character_name(message)
        elif('lsclearnick' == message.content):
            await clear_ls_nickname(message)

def start_log():
    try:
        logging_path = get_config_value('logging.path')
        if(logging_path is None):
            print('No logging path found. Aborting.')
            pause_exit(0, 'Press Any Key To Exit.')
        if(path.exists(logging_path) == True):
            root_directory = Path(logging_path)
            total_log_size = sum(f.stat().st_size for f in root_directory.glob('**/*') if f.is_file())
            if(total_log_size > 0):
                total_log_size /= 1_000_000
            logging_warn_size = get_config_value('logging.warning-size')
            if(logging_warn_size is not None and total_log_size >= logging_warn_size):
                print('Your log folder size has reached or exceeded the set warning value (' + str(total_log_size) + '). Consider deleting old logs.')
        else:
            raise Exception
        tm =  time.strftime('%Y-%m-%d-%H-%M-%S')
        config_logging_level = get_config_value('logging.level').lower()
        if(config_logging_level == 'info'):
            print('Setting Debug Level to INFO')
            logger.setLevel(logging.INFO)
        elif(config_logging_level == 'debug'):
            print('Setting Debug Level to DEBUG')
            logger.setLevel(logging.DEBUG)
        elif(config_logging_level == 'warning'):
            print('Setting Debug Level to WARNING')
            logger.setLevel(logging.WARNING)
        elif(config_logging_level == 'error'):
            print('Setting Debug Level to ERROR')
            logger.setLevel(logging.ERROR)
        elif(config_logging_level == 'critical'):
            print('Setting Debug Level to CRITICAL')
            logger.setLevel(logging.CRITICAL)
        else:
            print('Unknown logging level provided. Defaults to DEBUG. Read pythons documentation on supported logging levels: https://docs.python.org/3/howto/logging.html')
            logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler(filename=logging_path+tm+'.log', encoding='utf-8', mode='w')
        print('Log created at: ' + logging_path)
        handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
        logger.addHandler(handler)
    except Exception as exc:
        print('An error happened while setting up logging. Please check the console. Aborting.')
        print(exc)
        pause_exit(0, 'Press Any Key To Exit.')
    print('Logging was setup successfully!')

client.run(get_config_value('secret'))