import discord
import json
import requests
from bot_logging import logger
from bot_util import get_config_value
from item_info import get_item_basic_name, get_item_basic_id

async def eval_mb_input(bot_user, message):
    sub_string = ""
    if("marketboard" in message.content == True):
        sub_string = message.content[len('!marketboard'):len(message.content)].strip()
    else:
        sub_string = message.content[len('!mb'):len(message.content)].strip()
    try:
        data_center = sub_string.split(' ')[0]
        item_id = sub_string.split(' ')[1]
        item_result = {}
        if(item_id.isnumeric() == False):
            sub_string = sub_string[len(data_center):len(sub_string)].strip()
            item_result = get_item_basic_name(sub_string)
        else:
            item_result = get_item_basic_id(item_id)
        if(item_result is None):
            await message.channel.send('I didn\'t find an item with that name. Check your spelling and try again!')
            return
        request_data = requests.get(F'https://universalis.app/api/{data_center}/{item_result["id"]}?entries=10')
        json_data = request_data.json()
        if(len(json_data['listings']) == 0):
            await message.channel.send(F'I couldn\'t find any listings on the \'{data_center}\' data center with id \'{item_result["id"]}\'')
        else:
            index = 0
            embed=discord.Embed(title=F'{item_result["originalName"]}', url=F'https://universalis.app/market/{item_result["id"]}', description=F'{item_result["description"]}')
            embed.set_author(name="Universalis", url="https://universalis.app/", icon_url='https://universalis.app/i/universalis/universalis.png')
            embed.set_thumbnail(url=F'https://universalis.app/i/icon2x/{item_result["id"]}.png')
            embed.add_field(name='Average Prices', 
            value=F'Avg: {round(json_data["averagePrice"])}|NQ: {round(json_data["averagePriceNQ"])}|HQ: {round(json_data["averagePriceHQ"])}', 
            inline=False)
            server_names = []
            for obj in json_data['listings']:
                if(server_names.count(obj["worldName"]) == 0):
                    server_names.append(obj["worldName"])
                    embed.add_field(name=F'{obj["worldName"]} :earth_americas:', 
                    value=F'PPU: {obj["pricePerUnit"]} :moneybag:|Qty: {obj["quantity"]}|{"HQ" if obj["hq"] == True else "NQ"}|{obj["retainerName"]} :shopping_cart:', 
                    inline=False)
                    if(index == 20):
                        await message.channel.send('More than 20 sellers found. Limiting the query to the first 20.')
                        break
                    else:
                        index += 1
                else:
                    continue
            embed.set_footer(text='Data Provided by Universalis & XIVAPI. Borel is open source on GitLab.')
            await message.channel.send(embed=embed)
    except IndexError as indexErr:
        print('Insufficient parameters provided.')
        logger.exception(indexErr)
        await message.channel.send('Insufficient parameters provided. Needs to be !mb *datacenter* *item id* or !mb *datacenter* *item name*')
